import fs from "fs";
import Handlebars from "handlebars";
import random_word_slug from "random-word-slugs";
import Burrow from "burrow";

const { generateSlug } = random_word_slug;

let paste_template = fs.readFileSync("paste/paste.template").toString();
let template = Handlebars.compile(paste_template);

class Paste {
    constructor(collection, profile) {
        this.mongo = collection;
        this.profile = profile;
    }
    async get(slug) {
        let result = await this.mongo.find({ "slug": slug }).toArray();
        return result;
    }
    toString(data) {
        return template(data);
    }
    async paste(request, fingerprint, token, id) {
        let content = request.body;
        if(content) {
            let timestamp = new Date();
            let paste_data = { 
                fingerprint: fingerprint,
                content: content,
                caption: decodeURIComponent(token || "pasted content"),
                slug: id || generateSlug(),
                timestamp: timestamp,
                date: `${timestamp.getFullYear()}-${("0" + (timestamp.getMonth() + 1)).slice(-2)}-${("0" + timestamp.getDate()).slice(-2)}`
            };
            console.log("Pasting", paste_data);
            if(id) {
                const updated = await this.mongo.updateOne({ slug: id }, { $set: paste_data });
            } else {
                const result = await this.mongo.insertOne(paste_data);
            }
            return new Burrow.Response().redirect(`gemini://${request.host}/paste/${paste_data.slug}`);
        } else {
            console.log("Request input");
            return new Burrow.Response().input("/paste", "Paste content");
        }
    }
    router() {
        let router = new Burrow.Router();
        router.add("/", async (request) => {
            console.log("paste route picking up");
            return await this.paste(request, request.client_certificate.fingerprint, request.token);
        });
        
        router.add("/:slug", async request => {
            let result = await this.get(request.slug);
            if (result && result.length > 0) {
                let owner = await this.profile.get({ fingerprint: result[0].fingerprint });
                let data = Object.assign({}, owner[0], result[0]);
                data.host = request.host;
                console.log(data);
                if(request.client_certificate && result[0].fingerprint == request.client_certificate.fingerprint) {
                    data.me = true;
                }    
                return new Burrow.Response(this.toString(data));
            } else {
                return new Burrow.Response().fail(`No such paste: ${request.slug}`, Burrow.Response.NOT_FOUND);
            }
        });

                
        router.add("/:slug/raw", async request => {
            if(request.body) {
                return await this.paste(request, request.client_certificate.fingerprint, request.token, request.slug);
            } else {
                let result = await this.get(request.slug);
                if (result && result.length > 0) {
                    let response = new Burrow.Response(result[0].content);
                    response.mime = "text/plain";
                    return response;
                } else {
                    return new Burrow.Response.fail(`No such paste: ${request.slug}`, Burrow.Response.NOT_FOUND);
                }    
            }
        });

        return router;
    }    
}

export default Paste;