# SpacePaste

## How to submit a paste

If your client supports titan uploads, you can use that to submit a new paste. If you pass a value in the "token" field, it will be used as the preformatted block caption.
There are no rules or guidelines what goes into a caption - the language being pasted might be a good start if you're looking for inspiration.

The Titan upload endpoints are:

* titan://{{{host}}}/paste/ to upload a new paste
* titan://{{{host}}}/paste/<slug>/raw to edit a paste you own (identified by client certificate)

Alternatively, you can use a gemini input to submit a paste - Note that this limits the maximum size of your upload to what can fit on the URL, and most clients present an input as single line input, which is not particularly practical for this purpose.

### Clients known to support titan uploads

=> https://github.com/skyjake/lagrange Lagrange  
=> https://gitlab.com/armen138/gerbil Gerbil  
=> https://alexschroeder.ch/cgit/gemini-titan/about/ Titan for Bash  

## How to share pastes outside of gemini space 

SpacePaste does not offer a HTTP proxy or access to content over any other protocol. However, several gemini-to-http proxy services exist, and any of them can be used to share your pastes with people who can't or won't use a gemini client.

For example:

=> https://portal.mozz.us/gemini/{{{host}}}/ SpacePaste served by portal.mozz.us  
=> https://proxy.vulpes.one/gemini/{{{host}}}/ SpacePaste served by proxy.vulpes.one  