import fs from "fs";
import Handlebars from "handlebars";
import Burrow from "burrow";

let latest_template = fs.readFileSync("latest/latest.template").toString();

let template = Handlebars.compile(latest_template);

class Latest {
    constructor(db) {
        // this.users = db.collection("users");
        this.mongo = db.collection("pastes");
    }
    toString(data) {
        return template(data);
    }
    async pastes() {
        let result = await this.mongo.find().sort({_id:-1}).limit(50).toArray();
        return result;
    }
    router() {
        let router = new Burrow.Router();
        router.add("/", async (request) => {
            let promise = new Promise(async (resolve, reject) => {
                console.log(request);
                let pastes = await this.pastes();
                resolve(new Burrow.Response(this.toString({ pastes: pastes })));
            });
            return promise;
        });
        return router;
    }
}

export default Latest;
