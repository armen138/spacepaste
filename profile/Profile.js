import fs from "fs";
import Handlebars from "handlebars";
import random_word_slug from "random-word-slugs";
import Burrow from "burrow";

const { generateSlug } = random_word_slug;

let profile_template = fs.readFileSync("profile/profile.template").toString();
let error_template = fs.readFileSync("profile/error.template").toString();

let template = Handlebars.compile(profile_template);
let error = Handlebars.compile(error_template);

class Profile {
    constructor(db) {
        this.users = db.collection("users");
        this.pastes = db.collection("pastes");
    }
    toString(data) {
        return template(data);
    }
    generate(fingerprint) {
        let generated = {
            fingerprint: fingerprint,
            display_name: generateSlug()
        };
        this.users.insertOne(generated);
        return generated;
    }
    async get(query) {
        let result = await this.users.find(query).toArray();
        return result;
    }
    async pastes_for(fingerprint) {
        let result = await this.pastes.find({ "fingerprint": fingerprint }).sort({_id:-1}).limit(20).toArray();
        return result;
    }
    router() {
        let router = new Burrow.Router();
        router.add("/", async (request) => {
            let promise = new Promise(async (resolve, reject) => {
                console.log(request);
                let valid_certificate = request.client_certificate && request.client_certificate.fingerprint;
                if (valid_certificate) {
                    let result = await this.get({ fingerprint: request.client_certificate.fingerprint });
                    let pastes = await this.pastes_for(request.client_certificate.fingerprint);
                    let user = result[0];
                    if (result && result.length > 0) {
                        user.pastes = pastes;
                        user.me = true;
                        user.host = request.host;
                        resolve(new Burrow.Response(this.toString(user)));
                    } else {
                        let generated = this.generate(request.client_certificate.fingerprint);
                        generated.me = true;
                        generated.pastes = [];
                        generated.host = request.host;
                        resolve(new Burrow.Response(this.toString(generated)));
                    }
                } else {
                    resolve(new Burrow.Response().fail("You need a client certificate to have a profile", Burrow.Response.CERTIFICATE_REQUIRED));
                }
            });
            return promise;
        });

        router.add("/set_display_name", async request => {
            if (request.body && request.body !== "") {
                console.log("Request has body", request.body);
                let display_name = request.body;
                const user = await this.users.find({ display_name: display_name }).toArray();
                console.log(user.length, user[0]);
                if(user && user.length > 0) {
                    if (user[0].fingerprint == request.client_certificate.fingerprint) {
                        return new Burrow.Response(error({ message: "New display name should be different from the current display name"}));
                    } else {
                        return new Burrow.Response(error({ message: "Display names should be unique, as they can be used to refer to your profile"}));
                    }
                }
                const result = await this.users.updateOne({ fingerprint: request.client_certificate.fingerprint }, { $set: { display_name: display_name } });
                return new Burrow.Response().redirect("/profile");
            } else {
                let result = await this.users.find({ "fingerprint": request.client_certificate.fingerprint }).toArray();
                if (result && result.length > 0) {
                    return new Burrow.Response().input("/profile/set_display_name", "Enter a new display name (currently: " + result[0].display_name + ")");
                } else {
                    return new Burrow.Response().fail("Can't find record for certificate", Burrow.Response.CERTIFICATE_NOT_AUTHORIZED);
                }
            }
        });

        router.add("/:display_name", async (request) => {
            let promise = new Promise(async (resolve, reject) => {
                console.log(request);
                let valid_certificate = request.client_certificate && request.client_certificate.fingerprint;
                if (valid_certificate) {
                    let result = await this.get({ display_name: request.display_name });
                    let user = result[0];
                    if (user) {
                        user.host = request.host;
                        user.me = user.fingerprint === request.client_certificate.fingerprint;
                        let pastes = await this.pastes_for(user.fingerprint);
                        user.pastes = pastes;
                        if (result && result.length > 0) {
                            resolve(new Burrow.Response(this.toString(user)));
                        } else {
                            let generated = this.generate(request.client_certificate.fingerprint);
                            resolve(new Burrow.Response(this.toString(generated)));
                        }
                    } else {
                        resolve(new Burrow.Response().redirect("/profile"));
                    }
                }
            });
            return promise;
        });

        return router;
    }
}

export default Profile;
