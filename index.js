import fs from "fs";
import Burrow from "burrow";
import mongo from "mongodb";
import random_word_slug from "random-word-slugs";
import Handlebars from "handlebars";
import Profile from "./profile/Profile.js";
import Paste from "./paste/Paste.js";
import Latest from "./latest/Latest.js";
let mongo_host = process.env.MONGO_HOST || "mongo";
let index_template = fs.readFileSync("public/index.gmi").toString();
let template = Handlebars.compile(index_template);

const { generateSlug } = random_word_slug;

let client = new mongo.MongoClient(`mongodb://${mongo_host}:27017`);

async function main() {
    await client.connect();
    const db = client.db("spacepaste");
    const users = db.collection("users");
    const pastes = db.collection("pastes");  
    let result = await users.find().toArray();
    console.log(result);
    let profile = new Profile(db);
    let paste = new Paste(pastes, profile);
    let latest = new Latest(db);
    let gemini = new Burrow.Gemini(3005, 30);
    let router = new Burrow.Router();
    router.add("/index", request => new Burrow.Response().redirect("/"));
    router.add("/index.gmi", request => new Burrow.Response().redirect("/"));
    router.add("/", request => new Burrow.Response(template(request)));
    let static_router = new Burrow.StaticRouter("public");
    gemini.mount("/", router);
    gemini.mount("/profile", profile.router());
    gemini.mount("/paste", paste.router());
    gemini.mount("/latest", latest.router());
    gemini.mount("/", static_router);
    gemini.listen();
    return "SpacePaste started";
}

main().then(console.log).catch(console.error);
